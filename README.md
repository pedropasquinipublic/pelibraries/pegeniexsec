_______________________________________________
<div align="center">
peGENIExsec

A python library for reading GENIE cross-sections

Creation date: 24 JULY 2020          
Created by 

Pedro Pasquini      
<https:/inspirehep.net/authors/1467863>    
</div>

_______________________________________________

# Introduction
This file contains some useful functions to
read the xml output files from GENIE [1,2] and export 
the data in a table format.

# Instalation

To **install** you can use pip. 

Go to folder in terminal and run 
>> pip3 install -e ../pegeniexsec

To **uninstall** it just type
>> pip3 uninstall pegeniexsec

# Usage

This library defines two classes   
<ul>  pepartialxsec() and petotalxsec() </ul>

The important class is **petotalxsec** which contains a dictionary of partial cross-sections for each neutrino type.

For an example with usage, see example folder.

# References

[1] C. Andreopoulos, A. Bell, D. Bhattacharya, F. Cavanna, J. Dobson, S. Dytman, H. Gallagher, P. Guzowski, R. Hatcher and P. Kehayias,
''<em>The GENIE Neutrino Monte Carlo Generator</em>'',
<a href="http://dx.doi.org/10.1016/j.nima.2009.12.009">
Nucl. Instrum. Meth. A614, 87-104 (2010)</a>
[<a href="http://arxiv.org/abs/0905.2517">
arXiv:0905.2517</a> [hep-ph]].

[2] C. Andreopoulos, C. Barry, S. Dytman, H. Gallagher, T. Golan, R. Hatcher, G. Perdue and J. Yarba,
''<em>The GENIE Neutrino Monte Carlo Generator: Physics and User Manual</em>'',
[<a href="http://arxiv.org/abs/1510.05494">
arXiv:1510.05494</a> [hep-ph]].