#########################################################
### 				created by						  ### 
###				Pedro Pasquini						  ###
###		https://inspirehep.net/authors/1467863		  ###
### 												  ### 
### 				creation date					  ### 
### 				21st Oct 2022					  ### 
#########################################################

class PartialXsec:
	def __init__(self):
		self.target  = 'tgt:0000000000'
		self.Inttype = False
		self.Energy  = []
		self.xsec    = []
		self.nutype  = 0

	def print_info(self):
		pstring = 'Cross-section of' + self.nutype + ' into '+self.target+'\n'
		line = 	''.join(['-' for i in range(len(pstring))])
		print(line)
		print(line)
		print(pstring)
		print('  process:'+self.Inttype+'\n')
		print(line)
		print(line)
		print('\t E [GeV]\t | \t xsec [10^-38cm^2] ')
		print(line)
		for i in range(len(self.Energy)):
			print('\t {:.4f} \t | \t {:.4f} '.format(self.Energy[i],1.0e38*self.xsec[i]))	