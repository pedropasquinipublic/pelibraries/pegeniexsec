##################################################
##################################################
##												##
##												##
##                 created by 					##
##               Pedro Pasquini					##
##            Created: 31 March 2022			##
##    https://inspirehep.net/authors/1467863    ##
##												##
##												##
##################################################
##################################################

####################################################
####################################################
##   This code has an example on how to use 	  ##
##            pegeniexsec library 				  ##
####################################################
####################################################

import matplotlib.pyplot as plt ## library to plot
import numpy as np	
## this is my library to load the cross section
from pegeniexsec.totalxsec import TotalXsec


if __name__ == "__main__":



	inXsecFile = './xsec/anue_lowE_xsec_1H.xml' ## Input xml xsec file
	## which target I want to load.
	### Here I am loading only 1H since in my xml.
	targetName = 'tgt:1000010010' ## Example: O16: 'tgt:1000080160', H1: 'tgt:1000010010'
	
	## allocate the cross-section class
	my_Xsec = TotalXsec() 

	### Load the cross-section from file
	my_Xsec.load_genie_xml_xsec(inXsecFile,targetName)


	## now we list all the QES in the file
	my_Xsec.print_list_process("anu_e", proc_type = "QES")

	## this is a list of the process in the file if you want
	process_list = my_Xsec.xseclist["anu_e"].keys()
		
	## Since there is a N:2212;proc:Weak[CC],QES, we will get the table for that one
	EnergyList_CCQES = my_Xsec.xseclist["anu_e"]["N:2212;proc:Weak[CC],QES"].Energy
	XsecList_CCQES   = my_Xsec.xseclist["anu_e"]["N:2212;proc:Weak[CC],QES"].xsec

	## We might also want the total cross-section. So lets get it
	EnergyRange = np.linspace(0.01, 0.3, num= 50) ## in GeV
	TotalXsec_anue = my_Xsec.GetTotalxSecList("anu_e", EnergyRange)

	### Now lets plot the Xsec:
	fig, ax = plt.subplots()

	
	ax.plot(EnergyList_CCQES, XsecList_CCQES/1.0e-38, label = "CC-QES", c= 'blue')
	ax.plot(EnergyRange, TotalXsec_anue/1.0e-38, label = "Total", c= 'black')

	ax.legend(frameon=False, handlelength=1, columnspacing=0.)

	ax.set_xlabel(r"$E_\nu$ [GeV]", fontsize=15)
	## notice that partial_xsec.xsec is in cm^2, but I am dividing by 10^{-48} to make it easier to see.
	ax.set_ylabel(r'$\sigma$ [$10^{-38}$cm$^{2}$]', fontsize=15)


	ax.set_xlim(min(EnergyRange), max(EnergyRange))
	ax.set_yscale('log')

	plt.show()