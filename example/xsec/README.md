### About anue_lowE_xsec_1H.xml file

H1 Cross-Section for anu_e obtained from GENIE3.0 with tune G18_02a_00_000. See <a href="https://github.com/GENIE-MC">GENIE</a> for details.  
The pre-computed cross-sections can be found <a href="http://scisoft.fnal.gov/scisoft/packages/genie_xsec">here</a>.

Ref:.

[1] C. Andreopoulos, A. Bell, D. Bhattacharya, F. Cavanna, J. Dobson, S. Dytman, H. Gallagher, P. Guzowski, R. Hatcher and P. Kehayias,
''<em>The GENIE Neutrino Monte Carlo Generator</em>'',
<a href="http://dx.doi.org/10.1016/j.nima.2009.12.009">
Nucl. Instrum. Meth. A614, 87-104 (2010)</a>
[<a href="http://arxiv.org/abs/0905.2517">
arXiv:0905.2517</a> [hep-ph]].

[1] C. Andreopoulos, C. Barry, S. Dytman, H. Gallagher, T. Golan, R. Hatcher, G. Perdue and J. Yarba,
''<em>The GENIE Neutrino Monte Carlo Generator: Physics and User Manual</em>'',
[<a href="http://arxiv.org/abs/1510.05494">
arXiv:1510.05494</a> [hep-ph]].
