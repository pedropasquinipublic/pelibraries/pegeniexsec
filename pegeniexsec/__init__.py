"""
       -- pegeniexsec -- 

A python library for reading GENIE cross-sections
"""

# creation date 24 JULY 2020 
__version__ = "1.0.0"
__author__ = 'Pedro Pasquini'
__credits__ = 'https://inspirehep.net/authors/1467863'
