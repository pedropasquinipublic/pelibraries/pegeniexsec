#########################################################
### 				created by						  ### 
###				Pedro Pasquini						  ###
###		https://inspirehep.net/authors/1467863		  ###
### 												  ### 
### 				creation date					  ### 
### 				21st Oct 2022					  ### 
#########################################################

import xml.etree.ElementTree as ET
import re
from collections import defaultdict
import numpy as np
from pegeniexsec.partialxsec import PartialXsec
from scipy.interpolate import interp1d


class TotalXsec:
	def __init__(self):
		self.target = 'tgt:0000000000'
		self.xseclist = {}
		self.FACTOR = 3.8809e-28 # this converts Natural units to cm^2
		
	def _nutype(self, nu_name: str):
		if nu_name == "nu_e":
			return r'{\nu_e}'
		if nu_name == "nu_mu":
			return r'{\nu_\mu}'
		if nu_name == "nu_tau":
			return r'{\nu_\tau}'
		if nu_name == "anu_e":
			return r'{\overline\nu_e}'
		if nu_name == "anu_mu":
			return r'{\overline\nu_\mu}'
		if nu_name == "anu_tau":
			return r'{\overline\nu_\tau}'

		raise ValueError(f"No neutrino of type {nu_name}!")

	def print_list_process(self, nu_type, proc_type = 'All'):
		if nu_type in self.xseclist:
			print('Notice, N stands for the target it might be colliding at: p = 2212 and n = 2112\n')
			print('Below is the list of interaction types loaded for: ' +self._nutype(nu_type)+ '\n')
			if proc_type == 'All':
				for key in self.xseclist[nu_type]:
					print('\t ' + key)
			else: 
				all_keys = self.xseclist[nu_type].keys()
				is_there_key = False
				for key in all_keys:
					if proc_type in key:
						is_there_key = True
						print('\t ' + key)
				if not is_there_key:
					raise ValueError(f"No process {proc_type} loaded in this xsec!")
		else:
			print('Error! nu_type must be nu_e, nu_mu, nu_tau, anu_e, anu_mu, or anu_tau you typed: {}.\n')

	def load_genie_xml_xsec(self, filename, target, OLDSPLINE = False):
		del self.xseclist
		self.xseclist = {"nu_e": {}, "nu_mu": {}, "nu_tau": {}, "anu_e": {}, "anu_mu": {}, "anu_tau": {}}
		self.target = target

		tree = ET.parse(filename)
		root = tree.getroot()

		tabs=[]

		for i in range(6):
			tabs.append([])

		particle_name = "none"
		test  = False

		if OLDSPLINE:
			spline = root
		else:
			spline = root[0]

		for child in spline:
			name = child.get('name')

			if target in name:
				
				test=True

				if 'nu:12' in name:
					particle_name = "nu_e"
				if 'nu:14' in name:
					particle_name = "nu_mu"
				if 'nu:16' in name:
					particle_name = "nu_tau"
				if 'nu:-12' in name:
					particle_name = "anu_e"
				if 'nu:-14' in name:
					particle_name = "anu_mu"
				if 'nu:-16' in name:
					particle_name = "anu_tau"

				aux=[[],[]]
				for child2 in child:
					aux[0].append(float(child2.find('E').text)) 
					aux[1].append(self.FACTOR*float(child2.find('xsec').text))

				Inttype = re.search(target+';(.*);', name).group(1)

				self.xseclist[particle_name] = {**self.xseclist[particle_name],**{Inttype: PartialXsec()}
                }
				self.xseclist[particle_name][Inttype].target  = target
				self.xseclist[particle_name][Inttype].Inttype = Inttype
				self.xseclist[particle_name][Inttype].nutype  = self._nutype(particle_name)
				self.xseclist[particle_name][Inttype].Energy  = np.array(aux[0])
				self.xseclist[particle_name][Inttype].xsec    = np.array(aux[1])

		if not test or particle_name == "none":
			print("Error! No target cross-section found in file!")

	def GetTotalxSecList(self, nu_type: str, range : np.array):

		return_xsec = np.zeros(len(range))
		if nu_type in self.xseclist:
			for _, xsec in self.xseclist[nu_type].items():
				return_xsec = return_xsec + interp1d(xsec.Energy, xsec.xsec, fill_value = 0.0, bounds_error = False)(range)
		else:
			raise TypeError('Error! nu_type must be nu_e, nu_mu, nu_tau, anu_e, anu_mu, or anu_tau you typed: {}.\n')
		
		return return_xsec

	def GetSemiTotalxSecList(self, nu_type: str, range : np.array, xsecTag: str):

		return_xsec = np.zeros(len(range))
		if nu_type in self.xseclist:
			for Xsecnames, xsec in self.xseclist[nu_type].items():
				if xsecTag in Xsecnames:
				    return_xsec = return_xsec + interp1d(xsec.Energy, xsec.xsec, fill_value = 0.0, bounds_error = False)(range)
		else:
			raise TypeError('Error! nu_type must be nu_e, nu_mu, nu_tau, anu_e, anu_mu, or anu_tau you typed: {}.\n')
		
		return return_xsec
