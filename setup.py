from setuptools import setup


setup(
    name='pegeniexsec',
    version='1.0.0',    
    description='A python library for reading GENIE cross-sections',
    url='https://gitlab.com/pedropasquinipublic/pelibraries/pereferences',
    author='Pedro Pasquini',
    author_email='pedrosimpas@gmail.com',
    packages=['pegeniexsec'],
    install_requires=[ 
                      'numpy',
                      "scipy"              
                      ],

    classifiers=[
        'Intended Audience :: Science/Research', 
        'Operating System :: POSIX :: Linux',      
        'Programming Language :: Python :: 3.8',
    ],
)
